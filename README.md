# OpenML dataset: pol

https://www.openml.org/d/44008

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark. Original description: 
 
**Author**:   
**Source**: Unknown -   
**Please cite**:   

This is a commercial application described in Weiss & Indurkhya (1995). 
 The data describes a telecommunication problem. No further information
 is available.
 
 Characteristics: (10000+5000) cases, 49 continuous attributes 
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Original Source: The data in the original format can be obtained 
 from http://www.cs.su.oz.au/~nitin

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44008) of an [OpenML dataset](https://www.openml.org/d/44008). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44008/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44008/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44008/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

